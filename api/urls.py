from django.conf.urls import url
from api import views

urlpatterns = [
    url(r'^categories/$', views.CategoryListView.as_view(), name='category_list'),
    url(r'^categories/(?P<pk>\d+)/$', views.CategoryDetailView.as_view(), name='category_detail'),
    url(r'^categories/retailers/$', views.RetailerCategoryListView.as_view(), name='retailer_category_list'),
    url(r'^categories/retailers/(?P<pk>\d+)/$', views.RetailerCategoryDetailView.as_view(), name='retailer_category_detail'),
    url(r'^categories/mappings/$', views.CategoryMappingListView.as_view(), name='category_mapping_list'),
    url(r'^categories/mappings/(?P<pk>\d+)/$', views.CategoryMappingDetailView.as_view(), name='category_mapping_detail'),
    url(r'^categories/get_category_mapping/$', views.MappedCategoryView.as_view(), name='get_category'),
    url(r'^categories/create_category_mapping/$', views.MapCategoryView.as_view(), name='map_category'),
]
