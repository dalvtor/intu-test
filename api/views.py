from rest_framework import generics
from rest_framework.views import APIView, Response
from rest_framework import status
from categories.models import Category, RetailerCategory, CategoryMapping
from .serializers import CategorySerializer, RetailerCategorySerializer, CategoryMappingSerializer
from .response_manager import ResponseManager


class CategoryListView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetailView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class RetailerCategoryListView(generics.ListAPIView):
    queryset = RetailerCategory.objects.all()
    serializer_class = RetailerCategorySerializer


class RetailerCategoryDetailView(generics.RetrieveAPIView):
    queryset = RetailerCategory.objects.all()
    serializer_class = RetailerCategorySerializer


class CategoryMappingListView(generics.ListAPIView):
    queryset = CategoryMapping.objects.all()
    serializer_class = CategoryMappingSerializer


class CategoryMappingDetailView(generics.RetrieveAPIView):
    queryset = CategoryMapping.objects.all()
    serializer_class = CategoryMappingSerializer


class MappedCategoryView(APIView):
    def get(self, request):
        """
        This method returns the corresponding internal categories mapping to the retailers and retailer categories
        passed by parameters

        :param request: Request object
        :returns: JSON Response with the status, data and message of the corresponding request
        :raises ValueError: If the parameters' format is not right
        """
       
        retailer_category_ids = request.GET.get('retailer_category_id')

        # retailer_category_id must be present
        if not retailer_category_ids:
            response = ResponseManager().json_error_response('retailer_category_id parameter must be supplied')
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
     
        # We accept request batch mapping
        retailer_category_ids = retailer_category_ids.split(",")     
        # Parameters must be numeric
        try:
            retailer_categories = list(map(int, retailer_category_ids))
        except ValueError:
            response = ResponseManager().json_error_response('retailer_category_id parameter must be numeric')
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        # Filter retailer categories, return 404 if they don't exist
        retailer_categories = RetailerCategory.objects.filter(id__in=retailer_categories)
        if not retailer_categories:
            response = ResponseManager().json_error_response('Retailer category does not exist')
            return Response(response, status=status.HTTP_404_NOT_FOUND)

        # Get the mappings, return 404 if they don't exist
        category_mappings = CategoryMapping.objects.filter(retailer_category__in=retailer_categories)
        if not category_mappings:
            response = ResponseManager().json_error_response(
                'Retailer category exists, but we do not have an internal mapping for it')
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        categories = Category.objects.filter(categorymapping__in=category_mappings)

        response = ResponseManager().json_success_response(CategorySerializer(categories, many=True).data, None)
        return Response(response)


class MapCategoryView(APIView):

    def post(self, request):
        """
        This method maps a retailer category with an internal category

        :param request: Request object
        :returns: JSON Response with the status, data and message of the corresponding request
        :raises ValueError: If the parameters' format is not right
        """
        retailer_category = request.data.get('retailer_category_id')
        internal_category = request.data.get('internal_category_id')
        # Check we received all the parameters we need
        if not retailer_category or not internal_category:
            response = ResponseManager().json_error_response(
                'retailer_category_id and internal_category_id are required do the mapping')
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        # Also check that they are in the correct format
        try:
            retailer_category = int(retailer_category)
            internal_category = int(internal_category)
        except ValueError:
            response = ResponseManager().json_error_response('retailer_category_id and internal_category_id parameters must be numeric')
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        # Get the objects to do the mapping, or return error in case they don't exist
        try:
            category = Category.objects.get(id=internal_category)
            retailer_category = RetailerCategory.objects.get(id=retailer_category)
            # Is is possible that the category is already mapped to some other value, so let's check it
            if CategoryMapping.objects.filter(retailer_category=retailer_category).exists():
                response = ResponseManager().json_error_response('That retailer category is already mapped')
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
            mapping = CategoryMapping.objects.create(retailer_category=retailer_category, internal_category=category)
            response = ResponseManager().json_success_response(CategoryMappingSerializer(mapping).data,
                                                               'Category mapped successfully')
            return Response(response, status=status.HTTP_201_CREATED)

        except Category.DoesNotExist:
            response = ResponseManager().json_error_response('The specified internal category does not exist')
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        except RetailerCategory.DoesNotExist:
            response = ResponseManager().json_error_response('The specified retailer category does not exist')
            return Response(response, status=status.HTTP_404_NOT_FOUND)

